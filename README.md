# Full Stack Web Development

## Cuarto Curso NodeJS/ExpressJS/MongoDB.

### Tecnologías

-   [cookie-parser](https://www.npmjs.com/package/cookie-parser)
-   [debug](https://www.npmjs.com/package/debug)
-   [express](https://expressjs.com/)
-   [http-errors](https://www.npmjs.com/package/http-errors)
-   [moment](https://momentjs.com/)
-   [mongoose](https://mongoosejs.com/)
-   [morgan](https://www.npmjs.com/package/morgan)
-   [pug](https://www.npmjs.com/package/pug)
-   [request](https://www.npmjs.com/package/request)
-   [bcrypt](https://www.npmjs.com/package/bcrypt)
-   [jasmine](https://jasmine.github.io/)
-   [nodemon](https://nodemon.io/)
-   [nodemailer](https://nodemailer.com/about/)
