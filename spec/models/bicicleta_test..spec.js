const mongoose = require('mongoose');
const Bicicleta = require('../../models/bicicleta');

describe('Testing Bicicletas', function () {

    beforeEach(function (done) {
        const mongoDB = 'mongodb://localhost/testDB';
        mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true });
        var db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error:'));
        db.once('open', function () {
            console.log("Connection successfully");
            done();
        })
    });

    afterEach(function (done) {
        Bicicleta.deleteMany({}, function (err, success) {
            if (err) console.log(err);
            mongoose.disconnect(err);
            done();
        });
    });

    describe("Bicicleta.createInstance", () => {
        it("crea una instancia de Bicicleta", () => {
            const bici = Bicicleta.createInstance(1, "verde", "urbana", [-34.5, -54.8]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("urbana");
            expect(bici.ubicacion[0]).toBe(-34.5);
            expect(bici.ubicacion[1]).toBe(-54.8);
        });
    });

    describe("Bicicleta.allBicis", () => {
        it("comienza vacia", (done) => {
            Bicicleta.allBicis(function (err, bicis) {
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });


    describe("Bicicleta.add", () => {
        it("Agrega solo una bici", (done) => {
            const aBici = new Bicicleta({ code: 1, color: "verde", modelo: "urbana" });
            Bicicleta.add(aBici, function (err, newBici) {
                if (err) console.error(err);
                Bicicleta.allBicis(function (err, bicis) {
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);
                    done();
                });
            });
        });
    });

    describe("Bicicleta.findByCode", () => {
        it("debe devolver la bici con code 1", (done) => {
            Bicicleta.allBicis(function (err, bicis) {
                expect(bicis.length).toBe(0);

                const aBici = new Bicicleta({ code: 1, color: "verde", modelo: "urbana" });
                Bicicleta.add(aBici, function (err, newBici) {
                    if (err) console.error(erro);

                    const aBici2 = new Bicicleta({ code: 2, color: "azul", modelo: "ruta" });
                    Bicicleta.add(aBici2, function (err, newBici) {
                        if (err) console.error(err);
                        Bicicleta.findByCode(1, function (err, targetBici) {
                            expect(targetBici.code).toBe(aBici.code);
                            expect(targetBici.color).toBe(aBici.color);
                            expect(targetBici.modelo).toBe(aBici.modelo);

                            done();
                        })
                    })
                })
            })
        })
    })

    describe("Bicicleta.removeByCode", () => {
        it("debe eliminar la bici con code 1", (done) => {
            Bicicleta.allBicis(function (err, bicis) {
                expect(bicis.length).toBe(0);

                const aBici = new Bicicleta({ code: 1, color: "verde", modelo: "urbana" });
                Bicicleta.add(aBici, function (err, newBici) {
                    if (err) console.error(erro);

                    const aBici2 = new Bicicleta({ code: 2, color: "azul", modelo: "ruta" });
                    Bicicleta.add(aBici2, function (err, newBici) {
                        if (err) console.error(err);
                        Bicicleta.removeByCode(1, function (err, targetBici) {
                            expect(Bicicleta.allBicis.length).toBe(1);
                            done();
                        })
                    })
                })
            })
        })
    })
});