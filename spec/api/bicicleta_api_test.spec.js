const Bicicleta = require('../../models/bicicleta');
const mongoose = require('mongoose');
const request = require('request');
const server = require('../../bin/www');

describe('Bicicleta API', () => {
    beforeAll(function (done) {
        mongoose.disconnect()
        done();
    })

    beforeEach(function (done) {
        const mongoDB = 'mongodb://localhost/testDB';
        mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true });
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error:'));
        db.once('open', function () {
            console.log("Connection successfully");
            done();
        })
    });

    afterEach(function (done) {
        Bicicleta.deleteMany({}, function (err, success) {
            if (err) console.log(err);
            mongoose.disconnect(err);
            done();
        });
    });

    describe('GET BICICLETAS /', () => {
        it("Status 200", (done) => {
            request.get('http://localhost:3000/api/bicicletas', function (error, response, body) {
                const result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);
                done()
            })
        })
    })
    describe('POST BICICLETAS /create', () => {
        it("Status 201", (done) => {
            const headers = { 'content-type': 'application/json' };
            const aBici = '{"code": 10, "color": "azul", "modelo": "ruta", "lat": -35, "lng": -54 }';
            request.post({
                headers,
                url: 'http://localhost:3000/api/bicicletas/create',
                body: aBici
            }, function (error, response, body) {
                expect(response.statusCode).toBe(201);
                const bici = JSON.parse(body).bicicleta;
                expect(bici.color).toBe("azul");
                expect(bici.modelo).toBe("ruta");
                expect(bici.ubicacion[0]).toBe(-35);
                expect(bici.ubicacion[1]).toBe(-54);
                done();
            })

        })
    })
    describe('DELETE BICICLETAS /delete', () => {
        it("Status 204", (done) => {
            const headers = { 'content-type': 'application/json' };
            const nuevaBici = Bicicleta.createInstance(1, "rojo", "urbana", [-37.9736422, -57.5433955]);
            Bicicleta.add(nuevaBici);

            const id = '{ "id": 1 }';

            request.delete({
                headers,
                url: 'http://localhost:3000/api/bicicletas/delete',
                body: id
            }, function (error, response, body) {
                expect(response.statusCode).toBe(204);
                Bicicleta.allBicis((err, bicis) => {
                    expect(bicis.length).toBe(0);
                    done();
                })
            })

        })
    })

    describe('UPDATE BICICLETAS /update', () => {
        it("Status 204", (done) => {
            const headers = { 'content-type': 'application/json' };

            const nuevaBici = Bicicleta.createInstance(1, "rojo", "urbana", [-37.9736422, -57.5433955]);
            Bicicleta.add(nuevaBici);

            const updateData = '{"color": "azul", "modelo": "ruta"}';

            request.put({
                headers,
                url: 'http://localhost:3000/api/bicicletas/update/1',
                body: updateData
            }, function (error, response, body) {
                expect(response.statusCode).toBe(200);
                Bicicleta.findByCode(1, (err, bici) => {
                    if (err) console.log(err);

                    expect(bici.modelo).toBe("ruta");
                    expect(bici.color).toBe("azul");
                    expect(bici.ubicacion[0]).toBe(-37.9736422);
                    done();
                })
            })

        })
    })
})