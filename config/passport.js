const passport = require('passport');
const localStrategy = require('passport-local').Strategy;
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const FacebookTokenStrategy = require('passport-facebook-token');
const Usuario = require('../models/usuario');

passport.use(
    new FacebookTokenStrategy(
        {
            clientID: process.env.FACEBOOK_ID,
            clientSecret: process.env.FACEBOOK_SECRET,
        },
        function (accessToken, refreshToken, profile, done) {
            try {
                Usuario.findOneOrCreateByFacebook(profile, function (
                    err,
                    user
                ) {
                    if (err) console.log('err ' + err);
                    return done(err, user);
                });
            } catch (err2) {
                console.log(err2);
                return done(err2, null);
            }
        }
    )
);

passport.use(
    new localStrategy(function (email, password, done) {
        Usuario.findOne({ email }, function (err, user) {
            if (err) return done(err);
            if (!user)
                return done(null, false, {
                    message: 'Email no existe o incorrecto',
                });
            if (!user.validPassword(password))
                return done(null, false, { message: 'Password incorrect' });
            return done(null, user);
        });
    })
);

passport.use(
    new GoogleStrategy(
        {
            clientID: process.env.GOOGLE_CLIENT_ID,
            clientSecret: process.env.GOOGLE_CLIENT_SECRET,
            callbackURL: process.env.HOST + '/auth/google/callback',
        },
        function (accessToken, refreshToken, profile, cb) {
            console.log(profile);
            Usuario.findOneOrCreateByGoogle(profile, function (err, user) {
                return cb(err, user);
            });
        }
    )
);

passport.serializeUser(function (user, cb) {
    cb(null, user.id);
});

passport.deserializeUser(function (id, cb) {
    Usuario.findById(id, function (err, usuario) {
        cb(err, usuario);
    });
});

module.exports = passport;
