const Bicicleta = require("../models/bicicleta");

//Listar bicicletas en la vista
exports.bicicleta_list = function (req, res) {
    Bicicleta.allBicis((err, bicicletas) => {
        res.render('bicicletas/index', { bicis: bicicletas });
    })
}

//Mostrar vista de formulario para crear una bici
exports.bicicleta_create_get = function (req, res) {
    res.render('bicicletas/create');
}

//Agregar bici por medio de un formulario
exports.bicicleta_create_post = function (req, res) {
    const { id, color, modelo, lat, lng } = req.body;
    const bici = new Bicicleta(id, color, modelo, [lat, lng]);
    Bicicleta.add(bici);

    res.redirect("/bicicletas");
}

//Obtener datos de una bici para editar y mostrar en vista.
exports.bicicleta_update_get = function (req, res) {
    let bici = Bicicleta.findById(req.params.id);
    res.render('bicicletas/update', { bici });
}

//Modificar bicicleta a editar y volver al listado de bicis
exports.bicicleta_update_post = function (req, res) {
    let bici = Bicicleta.findById(req.params.id);
    const { id, color, modelo, lat, lng } = req.body;

    bici.id = id;
    bici.modelo = modelo;
    bici.color = color;
    bici.ubicacion = [lat, lng];

    res.redirect("/bicicletas");
}

//Eliminar bicileta por ID y volver al listado.
exports.bicicleta_delete_post = function (req, res) {
    Bicicleta.removeById(req.body.id);
    res.redirect('/bicicletas');
}